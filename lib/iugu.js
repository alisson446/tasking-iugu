const https = require('https');
const url = require('url');

module.exports = function iugu(method, apiUrl, requestBody) {

	let contentLength = 0;
	const { host, path } = url.parse(apiUrl);

	if (requestBody != null) {
		requestBody = JSON.stringify(requestBody);
		contentLength = Buffer.byteLength(requestBody);
	}

	const request = https.request({ 
    method: method,
    host: host,
    path: path,
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': contentLength
    },
    auth: '80e7ad30582be239c288d5325d3671df:'
  }, res => {

  	res.setEncoding('utf8');
	  res.on('data', (chunk) => {
	  	console.log(chunk);
	  });

  });

	if (method != 'GET') {
		request.write(requestBody);
	}

	request.end();

}
