const iugu = require('../../lib/iugu');

function createInvoices() {

  const url = 'https://api.iugu.com/v1/invoices';

  const requestBody = {
    email: 'alissonoliveira446@keeptasking.com',
    due_date: '19/07/2017',
    items: [ 
      { 
        description: 'fatura 1', 
        quantity: 1, 
        price_cents: 1000 
      }
    ]
  };

  iugu('POST', url, requestBody);
  
}

module.exports = {
  createInvoices
}
