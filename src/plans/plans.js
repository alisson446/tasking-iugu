const iugu = require('../../lib/iugu');

function createPlans() {

  const url = 'https://api.iugu.com/v1/plans';

  const requestBody = {
    name: 'plano mensal',
    identifier: 'free_plan',
    interval: 1,
    interval_type: 'months',
    value_cents: 0
  };

  iugu('POST', url, requestBody);
  
}

function listPlans() {

  const url = 'https://api.iugu.com/v1/plans';
  iugu('GET', url);

}

module.exports = {
  createPlans,
  listPlans
}
