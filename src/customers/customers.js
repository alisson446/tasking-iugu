const iugu = require('../../lib/iugu');

function createCustomer() {

  const url = 'https://api.iugu.com/v1/customers';

  const requestBody = {
    email: 'alissonoliveira446@keeptasking.com',
    name: 'Alisson Oliveira'
  };

  iugu('POST', url, requestBody);

}

function createCustomerPaymentMethod() {

  const customerId = '4698A517705248098BCC2ABE9C0B572D';
  const url = `https://api.iugu.com/v1/customers/${customerId}/payment_methods`;

  const requestBody = {
    description: 'cartão de crédito',
    item_type: 'credit_card',
    data: {
      holder_name: 'Alisson Oliveira',
      display_number: '1111-1111-1111-1111',
      brand: 'visa',
      month: 12,
      year: 2022
    }
  };

  iugu('POST', url, requestBody);

}

module.exports = {
  createCustomer,
  createCustomerPaymentMethod
}
