const iugu = require('../../lib/iugu');

function createWebhook() {
  
  const url = `https://api.iugu.com/v1/web_hooks`;

  iugu('POST', url, {
    event: 'all',
    url: 'http://localhost:3000/webhook'
  });
  
}

function listWebhooks() {
  
  const url = `https://api.iugu.com/v1/web_hooks`;
  iugu('GET', url);
  
}

module.exports = {
  createWebhook,
  listWebhooks
}
