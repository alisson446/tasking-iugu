const iugu = require('../../lib/iugu');

function createPaymentToken() {

  const url = 'https://api.iugu.com/v1/payment_token';

  const requestBody = {
    account_id: '80e7ad30582be239c288d5325d3671df',
    method: 'credit_card',
    test: true,
    data: {
      first_name: 'Alisson',
      last_name: 'Oliveira',
      number: '1111-1111-1111-1111',
      verification_value: 123,
      month: 12,
      year: 2022
    }
  };

  iugu('POST', url, requestBody);
  
}

module.exports = {
  createPaymentToken
}
