const iugu = require('../../lib/iugu');

function createSubscription() {

  const url = 'https://api.iugu.com/v1/subscriptions';

  const requestBody = {
    plan_identifier: 'free_plan',
    customer_id: '4698A517705248098BCC2ABE9C0B572D',
    credits_based: false,
    payable_with: 'all',
    subitems: [
      {
        description: 'item 1',
        price_cents: 50,
        quantity: 1,
        recurrent: true
      },
      {
        description: 'item 2',
        price_cents: 100,
        quantity: 2,
        recurrent: true
      },
      {
        description: 'item 3',
        price_cents: 1000,
        quantity: 3,
        recurrent: true
      }
    ]
  };

  iugu('POST', url, requestBody);

}

function activateSubscription() {
  
  const subscriptionId = '3979C04370574637AB02BC0DC00517E0';
  const url = `https://api.iugu.com/v1/subscriptions/${subscriptionId}/activate`;

  iugu('POST', url, {});
  
}

module.exports = {
  createSubscription,
  activateSubscription
}
