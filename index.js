const app = require('express')(); 
const { createCustomer, createCustomerPaymentMethod } = require('./src/customers/customers');
const { createInvoices } = require('./src/invoices/invoices');
const { listPlans, createPlans } = require('./src/plans/plans');
const { createPaymentToken } = require('./src/paymentToken/paymentToken');
const { createSubscription, activateSubscription } = require('./src/subscriptions/subscriptions');
const { createWebhook, listWebhooks } = require('./src/webhooks/webhooks');

app.post('/webhook', function (req, res) {
  console.log(req.body);
});

app.listen(3000, function() {
	createSubscription();
});
